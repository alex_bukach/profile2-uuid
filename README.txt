-- SUMMARY --

Since the UUID module provides UUIDs only for the core entities (nodes,
comments, taxonomy terms and user profiles), the Profile2 UUID module enables
the functionalities of the UUID module for the Profile2 entity type.

For a full description of the module, visit the project page:
  http://drupal.org/project/profile2_uuid

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/profile2_uuid


-- REQUIREMENTS --

The following projects must be enabled:
  - Profile 2 (http://drupal.org/project/profile2)
  - UUID (http://drupal.org/project/uuid)


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONTACT --

Current maintainers:
* Pierre Villette - http://drupal.org/user/2293492

This project has been sponsored by:
* Phéromone
  "Phéromone - Interaction agency" is a digital agency that provides custom web
  strategy, design and technology to its clients since 1994.
  Visit http://www.pheromone.ca for more information.
